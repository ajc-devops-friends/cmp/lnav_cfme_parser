# lnav_cfme_parser

Instructions and rules to parse CloudForms logs with Lnav

## Installing lnav on Centos 7

Using EPEL-repository

If not yet present, install the EPEL repository with:

    # yum install epel-release

and then install the utility:

    # yum install lnav

## Copy the template file into Lnav configuration directory

    # cp cfme_log.json  ~/.lnav/formats/installed/cfme_log.json
    
You can now use lnav to browse `evm.log`, `automation.log`, `api.log`